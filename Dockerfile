# base image with java runtime
FROM java:8

# get arguments for the specifi code version
ARG code_version

# pass environment variable
ENV code_version=$code_version

# maintainer for traceability
LABEL maintainer = "hitesh.jha@clusus.com"

# copy files to the container
COPY [ "target/assignment-$code_version.jar","entrypoint.sh", "/opt/" ]

#create and switch to a non root user
RUN useradd -U devuser
RUN chown -R devuser:devuser /opt/
USER devuser

#set working directoy
WORKDIR "/opt/"

# application runs on port 8090
EXPOSE 8090

# run the jar file
ENTRYPOINT ["/opt/entrypoint.sh"]