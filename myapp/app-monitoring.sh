helm install elastic/elasticsearch --version 7.9.0
helm install elastic/kibana --version 7.9.0
helm install elastic/filebeat --version 7.9.0
helm install my-prometheus-operator stable/prometheus-operator

